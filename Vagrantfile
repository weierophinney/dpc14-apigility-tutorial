# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "ubuntu/precise64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  #config.vm.network "forwarded_port", guest: 22,    host: 10122
  config.vm.network :forwarded_port, guest: 80,    host: 10190, auto_correct: true
  config.vm.network :forwarded_port, guest: 10081, host: 10191, auto_correct: true
  config.vm.network :forwarded_port, guest: 10082, host: 10192, auto_correct: true
  config.vm.network :forwarded_port, guest: 10083, host: 10193, auto_correct: true

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "private_network", type: "dhcp"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # If true, then any SSH connections made will enable agent forwarding.
  # Default value: false
  # config.ssh.forward_agent = true

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "./", "/home/vagrant/host"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Don't boot with headless mode
    # vb.gui = true
  
    # Use VBoxManage to customize the VM. For example to change memory:
    vb.customize ["modifyvm", :id, "--memory", "4096"]
  end
  #
  # View the documentation for the provider you're using for more
  # information on available options.


  # Shell provisioning
  # Get default Zend Server Web API key details
  # If they were passed in the environment, use those; otherwise, use defaults.
  ZS_WEB_API_KEY_NAME = ENV['ZS_WEB_API_KEY_NAME'] || "vagrant"
  ZS_WEB_API_KEY = ENV['ZS_WEB_API_KEY'] || "4425a15acfa126fde43c064a662383e4ed5d3588ee8e5e9cfcf1719ad7e78766"

  # Install required packages: curl, git, vim, python-pip
  config.vm.provision "shell" do |s|
    s.path = "vagrant/provision_scripts/packages.sh"
  end
  
  # Install web tools (HTTPie, jq)
  config.vm.provision "shell" do |s|
    s.path = "vagrant/provision_scripts/tools.sh"
  end

  # Run the Zend Server installer
  config.vm.provision "shell" do |s|
    s.inline = "host/vagrant/repo-installer-early-access/install_zs.sh 5.5 --automatic"
  end

  # Bootstrap Zend Server
  # If a Zend Server license order/key were both passed, they will be used to
  # configure the license when bootstrapping. You can also pass in the admin
  # password by setting it in the environment; if you do not, the default is
  # "admin".
  config.vm.provision "shell" do |s|
    ZS_ORDER_NUMBER = ENV['ZS_ORDER_NUMBER']
    ZS_LICENSE_KEY = ENV['ZS_LICENSE_KEY']
    ZS_ADMIN_PASSWORD = ENV['ZF_ADMIN_PASSWORD'] || "admin"

    s.path = "vagrant/provision_scripts/zend_server.sh"
    if ZS_ORDER_NUMBER.nil? or ZS_LICENSE_KEY.nil?
      s.args   = ["#{ZS_WEB_API_KEY_NAME}", "#{ZS_WEB_API_KEY}", "#{ZS_ADMIN_PASSWORD}"]
    else
      s.args   = ["#{ZS_WEB_API_KEY_NAME}", "#{ZS_WEB_API_KEY}", "#{ZS_ADMIN_PASSWORD}", "#{ZS_ORDER_NUMBER}", "#{ZS_LICENSE_KEY}"]
    end
  end
end
