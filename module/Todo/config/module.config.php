<?php
return array(
    'controllers' => array(
        'factories' => array(
            'Todo\\V1\\Rpc\\Ping\\Controller' => 'Todo\\V1\\Rpc\\Ping\\PingControllerFactory',
            'Todo\\V2\\Rpc\\Ping\\Controller' => 'Todo\\V2\\Rpc\\Ping\\PingControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'todo.rpc.ping' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/ping',
                    'defaults' => array(
                        'controller' => 'Todo\\V1\\Rpc\\Ping\\Controller',
                        'action' => 'ping',
                    ),
                ),
            ),
            'todo.rest.users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users[/:users_id]',
                    'defaults' => array(
                        'controller' => 'Todo\\V1\\Rest\\Users\\Controller',
                    ),
                ),
            ),
            'todo.rest.lists' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/lists/:lists_id',
                    'defaults' => array(
                        'controller' => 'Todo\\V1\\Rest\\Lists\\Controller',
                    ),
                ),
            ),
            'todo.rest.user-lists' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users/:user_id/lists',
                    'defaults' => array(
                        'controller' => 'Todo\\V2\\Rest\\UserLists\\Controller',
                    ),
                ),
            ),
            'todo.rest.tasks' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/lists/:lists_id/tasks[/:tasks_id]',
                    'defaults' => array(
                        'controller' => 'Todo\\V2\\Rest\\Tasks\\Controller',
                    ),
                ),
            ),
            'todo.rest.list-users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/lists/:lists_id/users[/:users_id]',
                    'defaults' => array(
                        'controller' => 'Todo\\V2\\Rest\\ListUsers\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'todo.rpc.ping',
            1 => 'todo.rest.users',
            2 => 'todo.rest.lists',
            3 => 'todo.rest.user-lists',
            4 => 'todo.rest.tasks',
            5 => 'todo.rest.list-users',
        ),
    ),
    'zf-rpc' => array(
        'Todo\\V1\\Rpc\\Ping\\Controller' => array(
            'service_name' => 'Ping',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'todo.rpc.ping',
        ),
        'Todo\\V2\\Rpc\\Ping\\Controller' => array(
            'service_name' => 'Ping',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'todo.rpc.ping',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Todo\\V1\\Rpc\\Ping\\Controller' => 'Json',
            'Todo\\V1\\Rest\\Users\\Controller' => 'HalJson',
            'Todo\\V1\\Rest\\Lists\\Controller' => 'HalJson',
            'Todo\\V2\\Rpc\\Ping\\Controller' => 'Json',
            'Todo\\V2\\Rest\\Users\\Controller' => 'HalJson',
            'Todo\\V2\\Rest\\Lists\\Controller' => 'HalJson',
            'Todo\\V2\\Rest\\UserLists\\Controller' => 'HalJson',
            'Todo\\V2\\Rest\\Tasks\\Controller' => 'HalJson',
            'Todo\\V2\\Rest\\ListUsers\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Todo\\V1\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.todo.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'Todo\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.todo.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Todo\\V1\\Rest\\Lists\\Controller' => array(
                0 => 'application/vnd.todo.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Todo\\V2\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'Todo\\V2\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Todo\\V2\\Rest\\Lists\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Todo\\V2\\Rest\\UserLists\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Todo\\V2\\Rest\\Tasks\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Todo\\V2\\Rest\\ListUsers\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Todo\\V1\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.todo.v1+json',
                1 => 'application/json',
            ),
            'Todo\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.todo.v1+json',
                1 => 'application/json',
            ),
            'Todo\\V1\\Rest\\Lists\\Controller' => array(
                0 => 'application/vnd.todo.v1+json',
                1 => 'application/json',
            ),
            'Todo\\V2\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/json',
            ),
            'Todo\\V2\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/json',
            ),
            'Todo\\V2\\Rest\\Lists\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/json',
            ),
            'Todo\\V2\\Rest\\UserLists\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/json',
            ),
            'Todo\\V2\\Rest\\Tasks\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/json',
            ),
            'Todo\\V2\\Rest\\ListUsers\\Controller' => array(
                0 => 'application/vnd.todo.v2+json',
                1 => 'application/json',
            ),
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'Todo\\LinkInjector' => 'Todo\\LinkInjectorListener',
        ),
        'factories' => array(
            'Todo\\AuthorizationListener' => 'Todo\\AuthorizationListenerFactory',
            'Todo\\ListAuthorization' => 'Todo\\ListAuthorizationFactory',
            'Todo\\V1\\Rest\\Users\\TableGateway' => 'Todo\\V1\\Rest\\Users\\TableGatewayFactory',
            'Todo\\V1\\Rest\\Users\\Mapper' => 'Todo\\V1\\Rest\\Users\\TableGatewayMapperFactory',
            'Todo\\V1\\Rest\\Users\\UsersResource' => 'Todo\\V1\\Rest\\Users\\UsersResourceFactory',
            'Todo\\V1\\Rest\\Lists\\ListsResource' => 'Todo\\V1\\Rest\\Lists\\ListsResourceFactory',
            'Todo\\V1\\Rest\\Lists\\Mapper' => 'Todo\\V1\\Rest\\Lists\\TableGatewayMapperFactory',
            'Todo\\V1\\Rest\\Lists\\TableGateway' => 'Todo\\V1\\Rest\\Lists\\TableGatewayFactory',
            'Todo\\V1\\Rest\\Lists\\UserListTableGateway' => 'Todo\\V1\\Rest\\Lists\\UserListTableGatewayFactory',
            'Todo\\V2\\Rest\\Users\\TableGateway' => 'Todo\\V2\\Rest\\Users\\TableGatewayFactory',
            'Todo\\V2\\Rest\\Users\\Mapper' => 'Todo\\V2\\Rest\\Users\\TableGatewayMapperFactory',
            'Todo\\V2\\Rest\\Users\\UsersResource' => 'Todo\\V2\\Rest\\Users\\UsersResourceFactory',
            'Todo\\V2\\Rest\\Lists\\ListsResource' => 'Todo\\V2\\Rest\\Lists\\ListsResourceFactory',
            'Todo\\V2\\Rest\\Lists\\Mapper' => 'Todo\\V2\\Rest\\Lists\\TableGatewayMapperFactory',
            'Todo\\V2\\Rest\\Lists\\TableGateway' => 'Todo\\V2\\Rest\\Lists\\TableGatewayFactory',
            'Todo\\V2\\Rest\\UserLists\\UserListsResource' => 'Todo\\V2\\Rest\\UserLists\\UserListsResourceFactory',
            'Todo\\V2\\Rest\\UserLists\\TableGateway' => 'Todo\\V2\\Rest\\UserLists\\TableGatewayFactory',
            'Todo\\V2\\Rest\\Tasks\\Mapper' => 'Todo\\V2\\Rest\\Tasks\\TableGatewayMapperFactory',
            'Todo\\V2\\Rest\\Tasks\\TableGateway' => 'Todo\\V2\\Rest\\Tasks\\TableGatewayFactory',
            'Todo\\V2\\Rest\\Tasks\\TasksResource' => 'Todo\\V2\\Rest\\Tasks\\TasksResourceFactory',
            'Todo\\V2\\Rest\\ListUsers\\ListUsersResource' => 'Todo\\V2\\Rest\\ListUsers\\ListUsersResourceFactory',
            'Todo\\V2\\Rest\\ListUsers\\Mapper' => 'Todo\\V2\\Rest\\ListUsers\\TableGatewayMapperFactory',
            'Todo\\V2\\Rest\\ListUsers\\TableGateway' => 'Todo\\V2\\Rest\\ListUsers\\TableGatewayFactory',
        ),
    ),
    'zf-rest' => array(
        'Todo\\V1\\Rest\\Users\\Controller' => array(
            'listener' => 'Todo\\V1\\Rest\\Users\\UsersResource',
            'route_name' => 'todo.rest.users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '10',
            'page_size_param' => null,
            'entity_class' => 'Todo\\V1\\Rest\\Users\\UsersEntity',
            'collection_class' => 'Todo\\V1\\Rest\\Users\\UsersCollection',
            'service_name' => 'Users',
        ),
        'Todo\\V1\\Rest\\Lists\\Controller' => array(
            'listener' => 'Todo\\V1\\Rest\\Lists\\ListsResource',
            'route_name' => 'todo.rest.lists',
            'route_identifier_name' => 'lists_id',
            'collection_name' => 'lists',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '10',
            'page_size_param' => null,
            'entity_class' => 'Todo\\V1\\Rest\\Lists\\ListsEntity',
            'collection_class' => 'Todo\\V1\\Rest\\Lists\\ListsCollection',
            'service_name' => 'Lists',
        ),
        'Todo\\V2\\Rest\\Users\\Controller' => array(
            'listener' => 'Todo\\V2\\Rest\\Users\\UsersResource',
            'route_name' => 'todo.rest.users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => array(
                0 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '10',
            'page_size_param' => '',
            'entity_class' => 'Todo\\V2\\Rest\\Users\\UsersEntity',
            'collection_class' => 'Todo\\V2\\Rest\\Users\\UsersCollection',
            'service_name' => 'Users',
        ),
        'Todo\\V2\\Rest\\Lists\\Controller' => array(
            'listener' => 'Todo\\V2\\Rest\\Lists\\ListsResource',
            'route_name' => 'todo.rest.lists',
            'route_identifier_name' => 'lists_id',
            'collection_name' => 'lists',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(),
            'collection_query_whitelist' => array(),
            'page_size' => '10',
            'page_size_param' => '',
            'entity_class' => 'Todo\\V2\\Rest\\Lists\\ListsEntity',
            'collection_class' => 'Todo\\V2\\Rest\\Lists\\ListsCollection',
            'service_name' => 'Lists',
        ),
        'Todo\\V2\\Rest\\UserLists\\Controller' => array(
            'listener' => 'Todo\\V2\\Rest\\UserLists\\UserListsResource',
            'route_name' => 'todo.rest.user-lists',
            'route_identifier_name' => 'lists_id',
            'collection_name' => 'lists',
            'entity_http_methods' => array(),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '10',
            'page_size_param' => null,
            'entity_class' => 'Todo\\V2\\Rest\\Lists\\ListsEntity',
            'collection_class' => 'Todo\\V2\\Rest\\Lists\\ListsCollection',
            'service_name' => 'UserLists',
        ),
        'Todo\\V2\\Rest\\Tasks\\Controller' => array(
            'listener' => 'Todo\\V2\\Rest\\Tasks\\TasksResource',
            'route_name' => 'todo.rest.tasks',
            'route_identifier_name' => 'tasks_id',
            'collection_name' => 'tasks',
            'entity_http_methods' => array(
                0 => 'PATCH',
                1 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '10',
            'page_size_param' => null,
            'entity_class' => 'Todo\\V2\\Rest\\Tasks\\TasksEntity',
            'collection_class' => 'Todo\\V2\\Rest\\Tasks\\TasksCollection',
            'service_name' => 'Tasks',
        ),
        'Todo\\V2\\Rest\\ListUsers\\Controller' => array(
            'listener' => 'Todo\\V2\\Rest\\ListUsers\\ListUsersResource',
            'route_name' => 'todo.rest.list-users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => array(
                0 => 'PATCH',
                1 => 'DELETE',
                2 => 'GET',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => '10',
            'page_size_param' => null,
            'entity_class' => 'Todo\\V2\\Rest\\ListUsers\\ListUsersEntity',
            'collection_class' => 'Todo\\V2\\Rest\\ListUsers\\ListUsersCollection',
            'service_name' => 'ListUsers',
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Todo\\V1\\Rest\\Users\\UsersEntity' => array(
                'entity_identifier_name' => 'user_id',
                'route_name' => 'todo.rest.users',
                'route_identifier_name' => 'users_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Todo\\V1\\Rest\\Users\\UsersCollection' => array(
                'entity_identifier_name' => 'user_id',
                'route_name' => 'todo.rest.users',
                'route_identifier_name' => 'users_id',
                'is_collection' => true,
            ),
            'Todo\\V1\\Rest\\Lists\\ListsEntity' => array(
                'entity_identifier_name' => 'list_id',
                'route_name' => 'todo.rest.lists',
                'route_identifier_name' => 'lists_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Todo\\V1\\Rest\\Lists\\ListsCollection' => array(
                'entity_identifier_name' => 'list_id',
                'route_name' => 'todo.rest.lists',
                'route_identifier_name' => 'lists_id',
                'is_collection' => true,
            ),
            'Todo\\V2\\Rest\\Users\\UsersEntity' => array(
                'entity_identifier_name' => 'user_id',
                'route_name' => 'todo.rest.users',
                'route_identifier_name' => 'users_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Todo\\V2\\Rest\\Users\\UsersCollection' => array(
                'entity_identifier_name' => 'user_id',
                'route_name' => 'todo.rest.users',
                'route_identifier_name' => 'users_id',
                'is_collection' => '1',
            ),
            'Todo\\V2\\Rest\\Lists\\ListsEntity' => array(
                'entity_identifier_name' => 'list_id',
                'route_name' => 'todo.rest.lists',
                'route_identifier_name' => 'lists_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Todo\\V2\\Rest\\Lists\\ListsCollection' => array(
                'entity_identifier_name' => 'list_id',
                'route_name' => 'todo.rest.lists',
                'route_identifier_name' => 'lists_id',
                'is_collection' => '1',
            ),
            'Todo\\V2\\Rest\\UserLists\\UserListsEntity' => array(
                'entity_identifier_name' => 'list_id',
                'route_name' => 'todo.rest.user-lists',
                'route_identifier_name' => 'user_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Todo\\V2\\Rest\\UserLists\\UserListsCollection' => array(
                'entity_identifier_name' => 'list_id',
                'route_name' => 'todo.rest.user-lists',
                'route_identifier_name' => 'user_id',
                'is_collection' => true,
            ),
            'Todo\\V2\\Rest\\Tasks\\TasksEntity' => array(
                'entity_identifier_name' => 'task_id',
                'route_name' => 'todo.rest.tasks',
                'route_identifier_name' => 'tasks_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Todo\\V2\\Rest\\Tasks\\TasksCollection' => array(
                'entity_identifier_name' => 'task_id',
                'route_name' => 'todo.rest.tasks',
                'route_identifier_name' => 'tasks_id',
                'is_collection' => true,
            ),
            'Todo\\V2\\Rest\\ListUsers\\ListUsersEntity' => array(
                'entity_identifier_name' => 'user_id',
                'route_name' => 'todo.rest.list-users',
                'route_identifier_name' => 'users_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'Todo\\V2\\Rest\\ListUsers\\ListUsersCollection' => array(
                'entity_identifier_name' => 'user_id',
                'route_name' => 'todo.rest.list-users',
                'route_identifier_name' => 'users_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-content-validation' => array(
        'Todo\\V1\\Rest\\Users\\Controller' => array(
            'input_filter' => 'Todo\\V1\\Rest\\Users\\Validator',
        ),
        'Todo\\V1\\Rest\\Lists\\Controller' => array(
            'input_filter' => 'Todo\\V1\\Rest\\Lists\\Validator',
        ),
        'Todo\\V2\\Rest\\Users\\Controller' => array(
            'input_filter' => 'Todo\\V2\\Rest\\Users\\Validator',
        ),
        'Todo\\V2\\Rest\\Lists\\Controller' => array(
            'input_filter' => 'Todo\\V2\\Rest\\Lists\\Validator',
        ),
        'Todo\\V2\\Rest\\UserLists\\Controller' => array(
            'input_filter' => 'Todo\\V2\\Rest\\UserLists\\Validator',
        ),
        'Todo\\V2\\Rest\\Tasks\\Controller' => array(
            'input_filter' => 'Todo\\V2\\Rest\\Tasks\\Validator',
        ),
        'Todo\\V2\\Rest\\ListUsers\\Controller' => array(
            'input_filter' => 'Todo\\V2\\Rest\\ListUsers\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Todo\\V1\\Rest\\Users\\Validator' => array(
            0 => array(
                'name' => 'username',
                'required' => true,
                'filters' => array(),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\EmailAddress',
                        'options' => array(),
                    ),
                ),
                'description' => 'Username (their email address)',
                'error_message' => 'Please provide a valid email address to use as the username.',
            ),
            1 => array(
                'name' => 'password',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '8',
                        ),
                    ),
                ),
                'description' => 'Password to use for this user.',
                'error_message' => 'Please provide a valid password of at least 8 characters in length.',
            ),
            2 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'The user\'s full name.',
                'error_message' => 'Please provide the user\'s name.',
            ),
        ),
        'Todo\\V1\\Rest\\Lists\\Validator' => array(
            0 => array(
                'name' => 'title',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'The title of the todo list.',
                'error_message' => 'Please provide a title for the todo list.',
            ),
        ),
        'Todo\\V2\\Rest\\Users\\Validator' => array(
            0 => array(
                'name' => 'username',
                'required' => '1',
                'filters' => array(),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\EmailAddress',
                        'options' => array(),
                    ),
                ),
                'description' => 'Username (their email address)',
                'error_message' => 'Please provide a valid email address to use as the username.',
            ),
            1 => array(
                'name' => 'password',
                'required' => '1',
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\StringLength',
                        'options' => array(
                            'min' => '8',
                        ),
                    ),
                ),
                'description' => 'Password to use for this user.',
                'error_message' => 'Please provide a valid password of at least 8 characters in length.',
            ),
            2 => array(
                'name' => 'name',
                'required' => '1',
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'The user\'s full name.',
                'error_message' => 'Please provide the user\'s name.',
            ),
        ),
        'Todo\\V2\\Rest\\Lists\\Validator' => array(
            0 => array(
                'name' => 'title',
                'required' => '1',
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'The title of the todo list.',
                'error_message' => 'Please provide a title for the todo list.',
            ),
        ),
        'Todo\\V2\\Rest\\UserLists\\Validator' => array(
            0 => array(
                'name' => 'title',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'Title for the todo list.',
                'error_message' => 'Please provide a title for the todo list.',
            ),
        ),
        'Todo\\V2\\Rest\\Tasks\\Validator' => array(
            0 => array(
                'name' => 'name',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                        'options' => array(),
                    ),
                ),
                'validators' => array(),
                'description' => 'The task name.',
                'error_message' => 'Please provide a task name.',
            ),
            1 => array(
                'name' => 'completed',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Boolean',
                        'options' => array(
                            'casting' => true,
                            'type' => 'all',
                        ),
                    ),
                ),
                'validators' => array(),
                'description' => 'Status of the task.',
                'continue_if_empty' => true,
                'error_message' => 'Please provide a boolean value indicating task completion status.',
            ),
        ),
        'Todo\\V2\\Rest\\ListUsers\\Validator' => array(
            0 => array(
                'name' => 'can_write',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Between',
                        'options' => array(
                            'min' => '0',
                            'max' => '1',
                        ),
                    ),
                ),
                'description' => 'Is the user allowed to write to the list?',
                'allow_empty' => true,
                'continue_if_empty' => true,
                'error_message' => 'Please provide a digit, 1 indicating "yes", or 0 indicating "no".',
            ),
            1 => array(
                'name' => 'can_read',
                'required' => true,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\Digits',
                        'options' => array(),
                    ),
                ),
                'validators' => array(
                    0 => array(
                        'name' => 'Zend\\Validator\\Between',
                        'options' => array(
                            'max' => '1',
                            'min' => '0',
                        ),
                    ),
                ),
                'description' => 'Is the user allowed read-access to the list?',
                'allow_empty' => true,
                'continue_if_empty' => true,
                'error_message' => 'Please provide a digit, 1 indicating "yes", or 0 indicating "no".',
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'Todo\\V1\\Rest\\Users\\Controller' => array(
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'Todo\\V1\\Rest\\Lists\\Controller' => array(
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => true,
                    'DELETE' => true,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'Todo\\V1\\Rpc\\Ping\\Controller' => array(
                'actions' => array(
                    'ping' => array(
                        'GET' => false,
                        'POST' => false,
                        'PATCH' => false,
                        'PUT' => false,
                        'DELETE' => false,
                    ),
                ),
            ),
            'Todo\\V2\\Rest\\Users\\Controller' => array(
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'Todo\\V2\\Rest\\Lists\\Controller' => array(
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => true,
                    'DELETE' => true,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'Todo\\V2\\Rpc\\Ping\\Controller' => array(
                'actions' => array(
                    'ping' => array(
                        'GET' => false,
                        'POST' => false,
                        'PATCH' => false,
                        'PUT' => false,
                        'DELETE' => false,
                    ),
                ),
            ),
            'Todo\\V2\\Rest\\UserLists\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
            'Todo\\V2\\Rest\\Tasks\\Controller' => array(
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PATCH' => true,
                    'PUT' => false,
                    'DELETE' => true,
                ),
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PATCH' => false,
                    'PUT' => false,
                    'DELETE' => false,
                ),
            ),
        ),
    ),
);
