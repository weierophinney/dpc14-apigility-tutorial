<?php
namespace Todo\V2\Rest\ListUsers;

use XTilDone\ListUsers\MapperInterface as ListUsersMapper;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ListUsersResource extends AbstractResourceListener
{
    protected $mapper;

    public function __construct(ListUsersMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $data = (array) $data;
        $filtered = $this->getInputFilter()->getValues();
    
        if (isset($data['username'])) {
            $filtered['username'] = $data['username'];
        }

        if (isset($data['user_id'])) {
            $filtered['user_id'] = $data['user_id'];
        }

        if (! isset($filtered['username']) && ! isset($filtered['user_id'])) {
            return new ApiProblem(400, 'Missing username and/or user identifier');
        }

        $identity = $this->getIdentity();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        return $this->mapper->create($identity->getRoleId(), $listId, $filtered);
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        $identity = $this->getIdentity();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        return $this->mapper->delete($identity->getRoleId(), $listId, $id);
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        $identity = $this->getIdentity();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        return $this->mapper->fetch($identity->getRoleId(), $listId, $id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        $identity = $this->getIdentity();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        return $this->mapper->fetchAll($identity->getRoleId(), $listId);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        $filtered = $this->getInputFilter()->getValues();
        $identity = $this->getIdentity();
        $listId   = $this->getEvent()->getRouteParam('lists_id', false);

        return $this->mapper->fetchAll($identity->getRoleId(), $listId, $id, $filtered);
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
