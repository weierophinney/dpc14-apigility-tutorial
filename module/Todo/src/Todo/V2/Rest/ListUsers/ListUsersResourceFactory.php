<?php
namespace Todo\V2\Rest\ListUsers;

class ListUsersResourceFactory
{
    public function __invoke($services)
    {
        return new ListUsersResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
