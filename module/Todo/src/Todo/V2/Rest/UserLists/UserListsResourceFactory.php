<?php
namespace Todo\V2\Rest\UserLists;

class UserListsResourceFactory
{
    public function __invoke($services)
    {
        return new UserListsResource($services->get('Todo\V2\Rest\Lists\Mapper'));
    }
}
