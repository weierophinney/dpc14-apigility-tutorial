<?php
namespace Todo\V2\Rest\Tasks;

class TasksResourceFactory
{
    public function __invoke($services)
    {
        return new TasksResource($services->get(__NAMESPACE__ . '\Mapper'));
    }
}
