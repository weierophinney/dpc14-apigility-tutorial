<?php
namespace Todo\V2\Rest\Tasks;

use ArrayObject;

class TasksEntity extends ArrayObject
{
    public function getArrayCopy()
    {
        return array(
            'task_id'   => $this['task_id'],
            'name'      => $this['name'],
            'completed' => (int) $this['completed'],
        );
    }
}
