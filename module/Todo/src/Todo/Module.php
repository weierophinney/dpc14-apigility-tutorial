<?php
namespace Todo;

use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'ZF\Apigility\Autoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }

    public function onBootstrap($e)
    {
        $app = $e->getTarget();
        $services = $app->getServiceManager();

        $events = $app->getEventManager();
        $events->attach('authorization', $services->get('Todo\AuthorizationListener'), 100);

        $sharedEvents = $services->get('SharedEventManager');
        $sharedEvents->attachAggregate($services->get('Todo\LinkInjector'));
    }
}
