<?php
namespace Todo;

use XTilDone\ListAuthorization;
use ZF\MvcAuth\Identity\AuthenticatedIdentity;

class AuthorizationListener
{
    protected $acls;

    protected $controllerRegex;

    protected $minVersion = 2;

    protected $permissions = array(
        'Lists::Entity::GET'          => ListAuthorization::IS_READ,
        'Lists::Entity::PUT'          => ListAuthorization::IS_WRITE,
        'Lists::Entity::DELETE'       => ListAuthorization::IS_ADMIN,
        'ListUsers::Collection::GET'  => ListAuthorization::IS_READ,
        'ListUsers::Collection::POST' => ListAuthorization::IS_ADMIN,
        'ListUsers::Entity::GET'      => ListAuthorization::IS_READ,
        'ListUsers::Entity::PATCH'    => ListAuthorization::IS_ADMIN,
        'ListUsers::Entity::DELETE'   => ListAuthorization::IS_ADMIN,
        'Tasks::Collection::GET'      => ListAuthorization::IS_WRITE,
        'Tasks::Collection::POST'     => ListAuthorization::IS_WRITE,
        'Tasks::Entity::DELETE'       => ListAuthorization::IS_WRITE,
        'Tasks::Entity::PATCH'        => ListAuthorization::IS_WRITE,
        'Users::Entity::GET'          => ListAuthorization::IS_READ,
    );

    protected $permissionTemplate = '%s::%s::%s';

    public function __construct(ListAuthorization $acls)
    {
        $this->acls = $acls;

        $ns = preg_quote('\\');
        $this->controllerRegex = vsprintf(
            '#^Todo%s+V(?P<version>[^%s]+)%s+(?:Rest|Rpc)%s(?P<controller>[^%s]+)%sController#',
            array_fill(0, 6, $ns)
        );
    }

    /**
     * Listen to the authorization event
     *
     * Fill in this method, as detailed in the inline comments of the method.
     * 
     * @param  \ZF\MvcAuth\MvcAuthEvent $mvcAuthEvent
     */
    public function __invoke($mvcAuthEvent)
    {
        $mvcEvent = $mvcAuthEvent->getMvcEvent();
        $request  = $mvcEvent->getRequest();
        if (! method_exists($request, 'getMethod')) {
            // Not an HTTP request; not worried
            return;
        }

        $routeMatch = $mvcEvent->getRouteMatch();
        if (! $routeMatch) {
            return;
        }

        $controller = $routeMatch->getParam('controller', '');

        if (! preg_match($this->controllerRegex, $controller, $matches)) {
            // Not a controller from this API
            return;
        }

        $controller = $matches['controller'];
        $version    = $matches['version'];

        if ($version < $this->minVersion) {
            return;
        }

        // @todo Get the request method and the current identity; assign to $method and $identity
        $method   = $request->getMethod();
        $identity = $mvcAuthEvent->getIdentity();

        if (in_array($matches['controller'], array('Users', 'UserLists'))) {
            $userId = $routeMatch->getParam('users_id', false);

            $lookup = sprintf(
                $this->permissionTemplate,
                $controller,
                ($userId ? 'Entity' : 'Collection'),
                $method
            );

            if (! isset($this->permissions[$lookup])) {
                // No permission schema; fallback to defaults
                return;
            }

            return $this->acls->authorizeUserAction(
                $this->permissions[$lookup],
                $userId,
                $identity->getRoleId()
            );
        }

        if ($matches['controller'] === 'Tasks') {
            $taskId = $routeMatch->getParam('tasks_id', false);
            $listId = $routeMatch->getParam('lists_id', false);

            $lookup = sprintf(
                $this->permissionTemplate,
                $controller,
                ($taskId ? 'Entity' : 'Collection'),
                $method
            );

            if (! isset($this->permissions[$lookup])) {
                // No permission schema; fallback to defaults
                return;
            }

            return $this->acls->authorizeListAction(
                $this->permissions[$lookup],
                $listId,
                $identity->getRoleId()
            );
        }

        $listId = $routeMatch->getParam('lists_id', false);
        $lookup = sprintf(
            $this->permissionTemplate,
            $controller,
            ($listId ? 'Entity' : 'Collection'),
            $method
        );

        if (! isset($this->permissions[$lookup])) {
            // No permission schema; fallback to defaults
            return;
        }
        return $this->acls->authorizeListAction(
            $this->permissions[$lookup],
            $listId,
            $identity->getRoleId()
        );
    }
}
